'''
Created on 1 august 2021
@author: cplumejeaud
ANR PORTIC : used to map geographic data of portic - ports, province and state borders of 1789
'''
from branca.utilities import color_brewer
from branca.element import Html

import folium
from folium.plugins import MarkerCluster
from folium.features import GeoJsonTooltip 

import pandas as pd
from sqlalchemy import create_engine, text as sql_text
import geopandas as gpd
import os

import branca
import json
import requests
import random

#from ipyleaflet import AwesomeIcon, GeoJSON, Map, Marker, LayerGroup, SearchControl


# Look at https://python-visualization.github.io/folium/quickstart.html#GeoJSON/TopoJSON-Overlays
# https://georgetsilva.github.io/posts/mapping-points-with-folium/
# https://makina-corpus.com/blog/metier/2019/python-carto
# https://www.geeksforgeeks.org/visualizing-geospatial-data-using-folium-in-python/?ref=rp 

# https://gis.stackexchange.com/questions/362582/coordinate-system-mismatch-in-folium 
# https://ipyleaflet.readthedocs.io/en/latest/api_reference/geo_json.html
# https://ipyleaflet.readthedocs.io/en/latest/api_reference/search_control.html

# For popup options, look at leaflet : https://leafletjs.com/reference-1.4.0.html#popup

url = (
    "https://raw.githubusercontent.com/medialab/portic-storymaps-2021/main/public/data/"
)
france_bad = f"{url}/cartoweb_france_1789_geojson.geojson"

url2 = (
    "https://gitlab.huma-num.fr/portic/gazetteer/-/raw/master/maps/"
)

url3 = (
    "D:/Dev/portic_humanum/gazetteer/maps"
)

world_geojson=f"{url3}/cartoweb_world_1789_29juillet2021_mixte4326_geojson.geojson"
ports_geojson=f"{url3}/ports_1789_4326.json"
ports_geojson="C:/Travail/Enseignement/Cours_M2_python/2023/code/resultats/ports_1789_4326.json"

cities_geojson=f"{url3}/cities_2018_4326.json"
admiralties_geojson=f"{url3}/limites_amirautes_4326.json"



"""
if not os.path.exists('world_1789.json'):
    r = requests.get(world_geojson)
    with open('world_1789.json', 'w') as f:
        f.write(r.content.decode("utf-8"))
"""
#with open('test.json', 'r') as f:
with open(world_geojson, 'r') as f:
    data = json.load(f)

#version 1 : read a dictionnary from a geojson
with open(ports_geojson, 'r', encoding='utf-8') as f:
    ports = json.load(f)
    print(type(ports))#dict

#version 2 : read a geodataframe with geopandas
ports = gpd.read_file(ports_geojson, encoding='utf-8')
print(type(ports))#geopandas.geodataframe.GeoDataFrame


#version 3 : read  a geodataframe from database with SQL Alchemy and geopandas
'''
connection = create_engine('postgresql://postgres:postgres@localhost:5432/portic_v10')
sql = "SELECT uhgs_id, latitude as lat, longitude as long, toponyme_standard_fr, amiraute, status, province, substate_1789_fr, state_1789_fr, geom FROM ports.port_points"
ports = gpd.GeoDataFrame.from_postgis(con=connection.connect(), sql=sql_text(sql))
'''

with open(cities_geojson, 'r') as f:
    cities = json.load(f)

with open(admiralties_geojson, 'r') as f:
    admiralties = json.load(f)

def random_color(feature):
    """A set of RGB colors has been randomly generated in the database for each unit of level 0 (state). """
    #print(feature['properties'])
    #print(feature['properties']['red'])
    return {
        'opacity': 1, 
        'dashArray': '9', 
        'fillOpacity': 1, 
        'weight': 1,
        'color': 'black',
        'fillColor': '#%02x%02x%02x' % (feature['properties']['red'], feature['properties']['green'], feature['properties']['blue']),
        #'fillColor': random.choice(['red', 'yellow', 'green', 'orange', 'blue']),
    }


#, tiles="Stamen Terrain",
'''
var Stadia_StamenWatercolor = L.tileLayer('https://tiles.stadiamaps.com/tiles/stamen_watercolor/{z}/{x}/{y}.{ext}', {
	minZoom: 1,
	maxZoom: 16,
	attribution: '&copy; <a href="https://www.stadiamaps.com/" target="_blank">Stadia Maps</a> &copy; <a href="https://www.stamen.com/" target="_blank">Stamen Design</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	ext: 'jpg'
});
https://watercolormaps.collection.cooperhewitt.org/#12/40.7846/-73.9577
<img class="leaflet-tile leaflet-tile-loaded" src="https://watercolormaps.collection.cooperhewitt.org/tile/watercolor/12/1209/1537.jpg" style="height: 256px; width: 256px; left: 1401px; top: -123px;">

'''
import xyzservices
import xyzservices.providers as xyz
public_provider = xyzservices.TileProvider (
    name="Stamen x Stadia maps",    
    url="https://tiles.stadiamaps.com/tiles/stamen_watercolor/{z}/{x}/{y}.jpg",
    attribution="(C) Stadia Maps - Stamen Design",
)
#print('public_provider.requires_token() ?')
#print(public_provider.requires_token())#False

stamen_free_provider = xyzservices.TileProvider (
    name="Stamen maps, hosted by Smithsnonian",    
    url="https://watercolormaps.collection.cooperhewitt.org/tile/watercolor/{z}/{x}/{y}.jpg",
    attribution="(C) Stamen Design",
)

m = folium.Map(location=[46.1722, -1.1481],zoom_start=6, tiles = stamen_free_provider, height='80%', top='0.0%', crs='EPSG3857')
#folium.TileLayer('Mapbox Bright').add_to(m)
#https://python-visualization.github.io/folium/latest/reference.html#module-folium.raster_layers
#http://leaflet-extras.github.io/leaflet-providers/preview/
#folium.raster_layers.TileLayer(tiles='https://tiles.stadiamaps.com/tiles/stamen_watercolor/{z}/{x}/{y}.jpg', min_zoom=1, max_zoom=16, attr='&copy; <a href="https://www.stadiamaps.com/" target="_blank">Stadia Maps</a> &copy; <a href="https://www.stamen.com/" target="_blank">Stamen Design</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors').add_to(m)
#tiles = folium.raster_layers.TileLayer(public_provider)
#tiles.add_to(m)
texte="<h1>Mapping the maritime world of 1789</h1>"
""" 	<h1>Mapping the maritime world of 1789</h1>
	<ul>
	<li>This map has been made in <a href='http://anr.portic.fr'> ANR PORTIC </a> under CC4.0-BY-SA Licence</li>
	<li>First layer is the borders of world in 1789, drawn from many sources. Ask to Christine Plumejeaud for metadata </li>
	<li>Second layer is the the set of harbors coming from navigocorpus database, for the end of 18th century. Ask to Silvia Marzagalli for metadata</li>
    <li>Third layer is the set of capitals cities by United Nations, Department of Economic and Social Affairs, Population Division (2018). World Urbanization Prospects: The 2018 Revision, Online Edition.</li>
    <li>Fourth layer is the limits of French admiralties, as described in Chardon report - 1786, and digitized in the ANR PORTIC project under CC4.0-BY-SA Licence</li>
	<li>Data and metadata can be downloaded from our <a href='https://gitlab.huma-num.fr/portic/gazetteer/maps'> github </a> </li>
	</ul> """
intro = folium.Div(height='20%', top='0.0%')
intro.add_child(Html(data=texte), name="intro")
m.get_root().add_child(intro,name='Christine',index=77)

style = {'fillColor': '#f5f5f5', 'lineColor': '#ffffbf', 'fill_opacity':'0.5','line_opacity': '.1'}
#styleMap = {'position': 'relative', 'width': '100%', 'height': '80.0%', 'left': '0.0%', 'top': '0.0%'}
#styleTitle = {'position': 'relative', 'width': '100%', 'height': '15.0%', 'left': '0.0%', 'top': '0.0%'}
#polygon = folium.GeoJson(gjson, style_function = lambda x: style).add_to(m)


#####################################################################
## First layer with countries
#####################################################################

"""folium.TopoJson(
    json.loads(requests.get(world_ok).text),
    "objects.cartoweb_world_1789_29juillet2021_mixte3857",
    #style_function=style_function,
    name="world_1789"
).add_to(m)"""

#https://ipyleaflet.readthedocs.io/en/latest/api_reference/geo_json.html
world_tooltips = GeoJsonTooltip(fields=('shortname',), labels=False, sticky=True) 

layer_world = folium.GeoJson(world_geojson,
    name="Word borders in 1789", 
    style_function= random_color,
    tooltip=world_tooltips
    )
layer_world.add_to(m)
marker_clusterinfo = MarkerCluster().add_to(layer_world)

feature_list = data["features"]
for f in feature_list:
    #print(f['properties'])
    shortname = f['properties']['shortname']
    latitude = f['properties']['lat']
    longitude = f['properties']['long']
    popuptext = "<b>"+shortname+"</b>"
    if f['properties']['dominant']!=None :
        if f['properties']['unitlevel']!=2:
            popuptext += "<br> Dominated by : "+str(f['properties']['dominant'])
        else:
            popuptext += "<br> Belongs to : "+str(f['properties']['dominant'])
    folium.Marker(
        [latitude, longitude], 
        popup=folium.Popup(html=popuptext,  maxWidth=500), 
        tooltip=shortname,
        icon=folium.Icon(color="red", icon="info-sign")
    ).add_to(marker_clusterinfo)

#####################################################################
## Second layer with ports
#####################################################################

layer_port = folium.FeatureGroup(name="Ports of navigocorpus database")
layer_port.add_to(m)

#https://georgetsilva.github.io/posts/mapping-points-with-folium/
marker_cluster = MarkerCluster().add_to(layer_port)



#For popup options, look at leaflet : https://leafletjs.com/reference-1.4.0.html#popup
if type(ports).__name__ == 'GeoDataFrame' : 
    for index, row in ports.iterrows():
        #print(row['uhgs_id'])
        shortname = row['toponyme_standard_fr']
        latitude = row['lat']
        longitude = row['long']
        popuptext = "<b>"+shortname+"</b>"
        if latitude!= None : 
            popuptext += "<br><table>"
            if row['state_1789_fr']!=None :
                popuptext += "<tr><td> State : </td><td>"+str(row['state_1789_fr']+"</td></tr>")
            if row['province']!=None :
                popuptext += "<tr><td> Province : </td><td>"+str(row['province']+"</td></tr>")
            if row['amiraute']!=None :
                popuptext += "<tr><td> Admiralty : </td><td>"+str(row['amiraute']+"</td></tr>")
            # if row['status']!=None :
            #     popuptext += "<tr><td> Status : </td><td>"+str(row['status']+"</td></tr>")
            # if row['uhgs_id']!=None :
            #     popuptext += "<tr><td> UHGS_ID : </td><td>"+str(row['uhgs_id']+"</td></tr>")
            popuptext += "</table>"
            folium.CircleMarker(
                [latitude, longitude], 
                popup=folium.Popup(html=popuptext,  maxWidth=500),
                radius=10,
                #fill=True,
                color="blue",
                color_opacity=0.5,
                fill_color="blue",
                fill_opacity=0.5,
                tooltip=shortname,
                #icon=folium.Icon(color="red", icon="info-sign")
            ).add_to(marker_cluster)
else :
    #when loaded as json dictionnary     
    feature_list = ports["features"]
    for f in feature_list:
        #print(f['properties'])
        #uhgs_id, toponyme_standard_fr, toponyme_standard_en, state_1789_fr, state_1789_en, amiraute, province, status
        shortname = f['properties']['toponyme_standard_fr']
        latitude = f['properties']['lat']
        longitude = f['properties']['long']
        popuptext = "<b>"+shortname+"</b>"
        if latitude!= None : 
            popuptext += "<br><table>"
            if f['properties']['state_1789_fr']!=None :
                popuptext += "<tr><td> State : </td><td>"+str(f['properties']['state_1789_fr']+"</td></tr>")
            if f['properties']['province']!=None :
                popuptext += "<tr><td> Province : </td><td>"+str(f['properties']['province']+"</td></tr>")
            if f['properties']['amiraute']!=None :
                popuptext += "<tr><td> Admiralty : </td><td>"+str(f['properties']['amiraute']+"</td></tr>")
            # if f['properties']['status']!=None :
            #     popuptext += "<tr><td> Status : </td><td>"+str(f['properties']['status']+"</td></tr>")
            # if f['properties']['uhgs_id']!=None :
            #     popuptext += "<tr><td> UHGS_ID : </td><td>"+str(f['properties']['uhgs_id']+"</td></tr>")
            popuptext += "</table>"
            folium.CircleMarker(
                [latitude, longitude], 
                popup=folium.Popup(html=popuptext,  maxWidth=500),
                radius=10,
                #fill=True,
                color="blue",
                color_opacity=0.5,
                fill_color="blue",
                fill_opacity=0.5,
                tooltip=shortname,
                #icon=folium.Icon(color="red", icon="info-sign")
            ).add_to(marker_cluster)


#https://ipyleaflet.readthedocs.io/en/latest/api_reference/search_control.html
""" marker = Marker(icon=AwesomeIcon(name="check", marker_color='green', icon_color='darkred'))
m.add_control(SearchControl(
  position="topleft",
  layer=layer_world,
  zoom=4,
  property_name='shortname',
  marker=marker
)) """


#####################################################################
## Cities layers
#####################################################################

layer_cities = folium.FeatureGroup(name="Cities (capitals) 2018")
layer_cities.add_to(m)

feature_list = cities["features"]
for f in feature_list:
    folium.vector_layers.CircleMarker(
        location=[f['properties']['lat'], f['properties']['long']],
        tooltip =f['properties']['capital_city'],
        radius = 7,
        color="black",
        fill_color="black",
        fill_opacity=0.5,
        #icon=folium.Icon(color="black", icon="info-sign")
    ).add_to(layer_cities)

#####################################################################
## Last layer with admiralties
#####################################################################

#https://ipyleaflet.readthedocs.io/en/latest/api_reference/geo_json.html
style_function_amiraute = lambda x: {'color': 'rgb(255, 0, 0)',  'weight': '3', 'dashArray': '20, 10', 'dashOffset': '20'}

#https://python-visualization.github.io/folium/modules.html#folium.features.GeoJsonTooltip
amiraute_tooltips = GeoJsonTooltip(fields=('admiralty1', 'admiralty2'), labels=False, sticky=False, 
    style=('background-color:grey; color:white;'))    
#, 'font-family:courier new; font-size:24px; padding:10px;'

layer_admiralities = folium.GeoJson(admiralties_geojson,
    name="Admiralties in 1789", 
    style_function = style_function_amiraute,
    smooth_factor = 1.2,
    tooltip=amiraute_tooltips,
    )
layer_admiralities.add_to(m)

#####################################################################
## Add controls
#####################################################################

## Add a control on layers to display
folium.LayerControl().add_to(m)
## You can get lat/long by clicking anywhere
m.add_child(folium.LatLngPopup())

##Export HTML
#m.save("maps/index.html")
m.save("map2023.html")