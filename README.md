

# 1. Portic gazetteer : get description of maritime places

Web API above the postgres database developped with Flask to get maritime places description, stored in schema ports

http://gaz.portic.fr/places?uhgs_id=


## 1.1. Installation

Neep Python 3 to be installed

Clone or download this git repo, then

pip3 install -r requirements.txt

Also, the lib difflib for python with npm: 
`npm install difflib`


At database level, the SGBD must have a base geonames. And you should provide a link through a materialized view with, and grant access to api_user for that.

```sql
create extension dblink;

drop  view ports.myremote_geonamesplaces;
create materialized VIEW ports.myremote_geonamesplaces AS
  SELECT *
    FROM dblink('dbname=geonames port=5433 user=postgres password=postgres options=-csearch_path=',
                'select geonameid::int, name, (feature_class||''.''||feature_code) as feature_code, alternatenames, country_code,  latitude::float, longitude::float , point3857
					from geonames.geonames_nov2019.allcountries a 
					where feature_class||''.''||feature_code in (select code from geonames.geonames_nov2019.feature_code where keep = ''x'')')
    AS t1(geonameid int, name text, feature_code text, alternatenames text , country_code text, latitude float, longitude float, point3857 geometry);
--- 34 s, 5 012 322 lignes
-- 37 s, 5 012 322 lignes

grant select on  ports.myremote_geonamesplaces to api_user;
grant select on  ports.matching_port to api_user;
grant select on  ports.limites_amirautes to api_user;
```

### 1.1.1. Running without Apache

python porticgaz.py

(or create a virtualenv, then activate it and pip install -r requirements && python apidata.py)

Backend is running and serve the data on http://localhost:80 (change the PORT variable at the top level of porticgaz.py)

### 1.1.2. Running with Apache

The gazetteer is served by Apache2 running WSGI module.

```bash

cd /home/plumegeo/navigo/Viz/gazetteer
git fetch origin master
git reset --hard origin/master
cd ..
cp gazetteer/porticgaz.py gazetteer/__init__.py

sudo chown :www-data /home/plumegeo/navigo/Viz/gazetteer/ -R
sudo chmod 755 /home/plumegeo/navigo/Viz/gazetteer/ -R
for fic in $(find /home/plumegeo/navigo/Viz/gazetteer/ -type f -name "*.py"); do sudo dos2unix $fic; done

sudo  service apache2 reload
```

## 1.2. List of possible requests

### 1.2.1. Principes

The published data  are an extraction of the geolocated points of call from navigocorpus for 1787, composed from the Marseille health register and G5 holidays, as well as the small cabotage notebooks for Marseille. 
These locations are retrieved from the [navigocorpus database](http://navigocorpus.org/) (Filemaker software) online, and also uploaded in a Postgres database using a program [ available for free in humanum gitlab](https://gitlab.huma-num.fr/portic/navigocorpus/-/tree/master/ETL). 

gazetteer's URL : **http://gaz.portic.fr/places/**


Three kind of requests : 
- **[ports](http://gaz.portic.fr/ports)** : a geojson file listing all available ports in the gazetteer
- **[places](http://gaz.portic.fr/places?)** : json_ld description of a place uniquely identified
- **[fieldnames](http://gaz.portic.fr/fieldnames)** : look for metadata on ports and maritime places with attributes list, their name and shorten name, their type and an explanation about the meaning. 

### 1.2.2. ports

Export list of ports_points (in 900013 projection or what is specified by user) in json format, 
    with all required attributes for visualisations (selection of parameters is not possible for the moment)
    
List of attributes : 
**ogc_fid, uhgs_id, total, toponym, belonging_states, belonging_substates, status, geonameid, admiralty, province, shiparea , point**
point is the geometry of the port.
User can get a description of the attributes by using /fieldnames?api=ports

Default srid is 900913
You get another by specifying a srid param

Will be extracted from postgres, schema ports, table port_points (see navigocorpus/ETL)
Tested alone in a browser:  
- http://gaz.portic.fr/ports?srid=4326 
- http://gaz.portic.fr/ports?
and by using explorex.portic.fr application (code alphaportic for visualisation) as client : it works

List of possible parameters for the request, with their default value in bold :
- srid : **900013** 
- format : csv | **json**
- shortenfields : true | **false**

**srid** is the projection used to deliver the geometries. Provide a valid EPSG (present in spatial_ref_sys table with postgis 2.5)

**format** to download data either in CSV format either in JSON format. 
For example : 
- <http://gaz.portic.fr/ports/?format=csv>
Semi-comma is the separator in CSV file (;)

To get same data in JSON : 
- <http://gaz.portic.fr/ports/?format=json>


**shortenfields** allow for shorten attribute names and thus to lighten size of downloaded data. With the API, you can have the mapping between names and shorten names : <http://gaz.portic.fr/fieldnames/?format=json>
Example : 
<http://gaz.portic.fr/ports?format=json&shortenfields=true>

Samples of what could be downloaded is in the file samples/ports.json

### 1.2.3. places


Return the places using linked places model as specified in 
    https://github.com/LinkedPasts/linked-places
    See : http://linkedpasts.org/ontology/lpo_latest.ttl



You can export 3 kind of entities for the moment. 
- Ports (geometries : Point)
- Admiralty (geometries : 2 MultiLineString )
- Province (geometries : set of belongings ports Points )

Here is the total list of entities, with the description template according to their kind and the source.

| Sources     	| Geo_general                                                                           	| Chardon                                                              	| Cassini                          	| Seignelay                                                            	| Amirauté                                	| Province                              	|
|-------------	|---------------------------------------------------------------------------------------	|----------------------------------------------------------------------	|----------------------------------	|----------------------------------------------------------------------	|-----------------------------------------	|---------------------------------------	|
|             	|                                                                                       	|                                                                      	|                                  	|                                                                      	|                                         	|                                       	|
| Cardinalité 	| 1250                                                                                  	| 86                                                                   	| 99                               	| 203                                                                  	| 51                                      	| 12                                    	|
| Identifiant 	| UHGS_id                                                                               	| Un identifiant sur 8 digits Rxxxxxxx                                 	| Un identifiant sur 8 digits      	| Un identifiant sur 8 digits                                          	| Nom de l’amirauté                       	| Nom de la province                    	|
|             	|                                                                                       	|                                                                      	| Txxxxxxx                         	| Sxxxxxxx                                                             	|                                         	|                                       	|
| When        	| 1749-1815   ou +                                                                      	| 1781-1785   ou +                                                     	| 1756   – 1790 ou +               	| 1681   – 1686 ou +                                                   	| * -1792                                 	| *-1791                                	|
| Names       	| Toutes les différentes appellations (sauf des   différences de casse)                 	|                                                                      	|                                  	|                                                                      	| 1 seul                                  	| 1 seul                                	|
| Types       	| Aat : ports (settlements)                                                             	|                                                                      	|                                  	|                                                                      	| Aat : former   administrative divisions 	| Aat : province                        	|
|             	|                                                                                       	|                                                                      	|                                  	|                                                                      	| 1669? -   1792                          	| Wiki :   wiki:Q209495                 	|
|             	|                                                                                       	|                                                                      	|                                  	|                                                                      	|                                         	| historical province of France         	|
|             	|                                                                                       	|                                                                      	|                                  	|                                                                      	|                                         	| 843 - 1791                            	|
| Links       	| Geonames                                                                              	|                                                                      	|                                  	|                                                                      	| Wikipedia                               	|                                       	|
|             	| closeMatch si   plusieurs appariements equivalents dans GeoNames,   exactMatch sinon. 	|                                                                      	|                                  	|                                                                      	| subjectOf                               	|                                       	|
|             	| Autre point du gazetier                                                               	|                                                                      	|                                  	|                                                                      	| primaryTopicOf                          	|                                       	|
|             	| -        Seignelay                                                                    	|                                                                      	|                                  	|                                                                      	|                                         	|                                       	|
|             	| -        Cassini                                                                      	|                                                                      	|                                  	|                                                                      	|                                         	|                                       	|
|             	| -        Chardon                                                                      	|                                                                      	|                                  	|                                                                      	|                                         	|                                       	|
| Relations   	| Relations d’appartenance                                                              	|                                                                      	|                                  	|                                                                      	| Royaume de France : * - 1792            	| Royaume de France :  * - 1791         	|
|             	| (broaderPartitive)                                                                    	|                                                                      	|                                  	|                                                                      	| France (geonames : 3017382)             	| France (geonames :   3017382)         	|
|             	| Amiraute : 1781-1785                                                                  	|                                                                      	|                                  	|                                                                      	| 1792-2020                               	| 1792-2020                             	|
|             	| Province : 1781-1785                                                                  	|                                                                      	|                                  	|                                                                      	|                                         	|                                       	|
|             	| Etat : *-*                                                                            	|                                                                      	|                                  	|                                                                      	|                                         	|                                       	|
| Positions   	| Celle de la source :   1749-1815                                                      	| Celle de la source :   1781-1785                                     	| Celle de la source :   1756-1790 	| Celle de la source :   1681-1686                                     	| Les géométries linéaires                	| Liste des points dans la   province : 	|
|             	|                                                                                       	|                                                                      	| Uncertain                        	|                                                                      	| Citation : le rapport de   Chardon      	| GeometryCollection                    	|
|             	| Celle de   Geoname (citée) : 1749– 2000                                               	| Celle de Geoname (citée) :   1781– 2000                              	| Celle de Geonames                	| Celle de Geoname (citée) :   1681– 2000                              	| When : 1781-1785                        	|                                       	|
|             	| uncertain   si  > 10 km des côtes   maritimes et fluviales                            	| uncertain si  > 10 km des côtes   maritimes et fluviales             	| (citée) : 1756– 2000             	| uncertain si  > 10 km des côtes   maritimes et fluviales             	| Uncertain                               	|                                       	|
|             	| Certain   sauf si plusieurs appariements equivalents dans GeoNames,.                  	| Certain sauf si plusieurs   appariements equivalents dans GeoNames,. 	|                                  	| Certain sauf si plusieurs   appariements equivalents dans GeoNames,. 	|                                         	|                                       	|


Entities will be extracted from postgres, schema navigoviz, table ports.port_points (see navigocorpus/ETL)

- You can get a province description : 
    http://gaz.portic.fr/places/?Flandre
- You can get a amiraute description : 
    http://gaz.portic.fr/places/?Dunkerque
- You can get a port description (only by its UHGS id): 
    http://gaz.portic.fr/places/?A0198999

You can go on http://explorex.portic.fr to find the UHGS id of a port present in the database, through a search form with a map to locate it. 



### 1.2.4. fielnames

Get metadata about the gazetteer with short and long name, type and definition, in English.

Api is 'ports' by default.

- http://gaz.portic.fr/fieldnames/?format=json
- http://gaz.portic.fr/fieldnames/?format=json&shortenfields=true
- http://gaz.portic.fr/fieldnames/?format=json&shortenfields=true&api=ports
- http://gaz.portic.fr/fieldnames/?format=csv&shortenfields=true&api=ports

Here is the UML model of a port entity in this database : 
![ClassDiagram1.png](ClassDiagram1.png)

# Liste of Amirautés and Provinces

| amirauté                | province   |
|-------------------------|------------|
| Dunkerque               | Flandre    |
| Calais                  | Picardie   |
| Boulogne                | Picardie   |
| Abbeville               | Picardie   |
| Saint-Valéry-sur-Somme  | Picardie   |
| Eu et Tréport           | Normandie  |
| Dieppe                  | Normandie  |
| Saint-Valéry-en-Caux    | Normandie  |
| Fécamp                  | Normandie  |
| Havre                   | Normandie  |
| Caudebec et Quilleboeuf | Normandie  |
| Rouen                   | Normandie  |
| Honfleur                | Normandie  |
| Touques                 | Normandie  |
| Dives                   | Normandie  |
| Caen                    | Normandie  |
| Bayeux                  | Normandie  |
| Isigny                  | Normandie  |
| La Hougue               | Normandie  |
| Barfleur                | Normandie  |
| Cherbourg               | Normandie  |
| Port-Bail               | Normandie  |
| Coutances               | Normandie  |
| Granville               | Normandie  |
| Saint-Malo              | Bretagne   |
| Saint-Brieuc            | Bretagne   |
| Morlaix                 | Bretagne   |
| Brest                   | Bretagne   |
| Quimper                 | Bretagne   |
| Vannes                  | Bretagne   |
| Nantes                  | Bretagne   |
| Les Sables d'Olonnes    | Poitou     |
| La Rochelle             | Aunis      |
| Marennes                | Saintonge  |
| Bordeaux                | Guyenne    |
| Bayonne                 | Guyenne    |
| Antibes                 | Provence   |
| Fréjus                  | Provence   |
| Saint-Tropez            | Provence   |
| Toulon                  | Provence   |
| La Ciotat               | Provence   |
| Marseille               | Provence   |
| Martigues               | Provence   |
| Arles                   | Provence   |
| Aigues-Mortes           | Languedoc  |
| Sète                    | Languedoc  |
| Agde                    | Languedoc  |
| Narbonne                | Languedoc  |
| Collioure               | Roussillon |
| Bastia                  | Corse      |
| Ajaccio                 | Corse      |
